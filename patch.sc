var squareGroup, sawGroup, noiseGroup, audio;
var filter, synthMod, synthBus, filterMod, filterBus;

~env = Group.new();
~osc = Group.after(~env);
~lfo = Group.after(~env);
~numVoices = 8;

synthBus = Bus.control;
filterBus = Bus.control;

~synthMod = Synth.head(~lfo, \lfo, [\bus, synthBus, \freq, 2,
	\depth, 0, \res, 2]);
~filterMod = Synth.head(~lfo, \lfo, [\bus, filterBus, \freq, 2,
	\depth, 0, \res, 2]);

squareGroup = Group.head(~osc);
sawGroup = Group.head(~osc);
noiseGroup = Group.after(~env);
audio = Bus.audio(s, 2);

~voices = ~numVoices.collect{
	var group, audio, amp, env, square, saw, noise;
	group = Group.head(~osc);
	amp = Bus.control;
	env = Synth.head(~env, \envelope, [\bus, amp, \attack, 5, \decay, 0,
		\sustain, 1, \release, 10]);
	square = Synth.head(group, \squareOsc, [\bus, audio, \freq, 50]);
	saw = Synth.head(group, \sawOsc, [\bus, audio, \freq, 50]);
	noise = Synth.head(noiseGroup, \noise, [\bus, audio, \amp, 0.1]);
	[square, saw]. do { |i|
		i.map(\amp, amp);
		i.map(\lfo, synthBus);
	};
	noise.map(\gain, amp);

	(osc: group, env: env);
};

~filter = Synth.after(~osc, \filter, [\bus, ~audio, \freq, 800]);
~filter.map(\lfo, filterBus);
~filter.set(\lfoAmt, 0.3);

// ~main = Synth.tail(s, \play, [\bus, ~audio, \amp, 0.3]);

~env.set(\attack, 30);
~env.set(\release, 30);
~filter.set(\freq, 100 + (0.76 * 2000));
~filter.set(\res, 0.14 * 4);
~filter.set(\lfoAmt, 0.985);
~osc.set(\lfoAmt, 0.0125);
~exp = [0.01,40,\exponential, 1/1000].asSpec;
~lfo.set(\depth, ~exp.map(0.26) - 0.01);
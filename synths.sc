SynthDef(\squareOsc, { |bus, freq, amp, lfo, lfoAmt |
	Out.ar(bus, 2 * Pulse.ar(freq + (freq * lfo * lfoAmt), 0.5, amp) ! 2);
}).add;

SynthDef(\sawOsc, { |bus, freq, amp, lfo, lfoAmt |
	Out.ar(bus, 2 * Saw.ar(freq + (freq * lfo * lfoAmt), amp) ! 2);
}).add;

SynthDef(\noise, { |bus, amp, gain|
	Out.ar(bus, 2 * WhiteNoise.ar * amp * gain ! 2);
}).add;


SynthDef(\lfo, { |bus, freq, depth|
	Out.kr(bus, SinOsc.kr(freq) * depth);
}).add;

SynthDef(\filter, { |bus, freq=2000, res=0, lfo, lfoAmt|
	var sig = In.ar(bus, 2);
	sig = MoogFF.ar(sig, freq + (freq * lfo * lfoAmt), res);
	ReplaceOut.ar(bus, sig);
}).add;

SynthDef(\delay, { |bus, freq=2000, res=0, lfo, lfoAmt|
	var sig = In.ar(bus, 2);
	sig = MoogFF.ar(sig, freq + (freq * lfo * lfoAmt), res);
	ReplaceOut.ar(bus, sig);
}).add;

SynthDef(\envelope, {|bus, gate, attack, decay, sustain, release, max=0.3|
	ReplaceOut.kr(bus,
		max * EnvGen.ar(Env.adsr(attack, decay, sustain, release), gate));
}).add;

SynthDef(\play, {|bus, amp|
	Out.ar([0, 1], amp * In.ar(bus, 2));
}).add;

SynthDef(\rec, { |bufnum, level=1, pre=0.75|
	var in = SoundIn.ar(0) * level;
	RecordBuf.ar(in, bufnum, doneAction: 2, loop: 0, preLevel: pre);
}).add;

SynthDef(\play, { |out = 0, bufnum = 0, loop = 0, speed=1|
	var playbuf;
	playbuf = PlayBuf.ar(1,bufnum, loop: loop, rate: [speed, speed * 1.005]);
	//	FreeSelfWhenDone.kr(playbuf); // frees the synth when the PlayBuf is finished
	Out.ar(out, playbuf);
}).add;

SynthDef(\eideticSynth, { |freq, amp=1, vol=1, gate=0, mod=0, overtones=0, port=0,
	                       vibRate=0, vibDepth=0, attack=0, release=0,
	delayMix=0.2|
	var n = 12, out, env, freqWithOvertones;
	freq = LFTri.ar(vibRate * 50, 0, vibDepth * 50, freq);
	freq = Lag.ar(freq, port);
	freqWithOvertones = Lag.ar(freq * ((overtones * 19) + 1).ceil, port / 4);
	env = EnvGen.kr(Env.adsr(attack,1,1,release), gate);
	out = Mix.fill(n, {arg x;
		[
			SinOsc.ar(freqWithOvertones +
				SinOsc.ar(10.rand * 0.001, 0, mod * 20.rand), 0, 1/n * env * amp),
				Saw.ar(freq + SinOsc.ar(10.rand * 0.001, 0, mod * 20.rand),
					1/n * amp * env),
			Pulse.ar(freq + SinOsc.ar(10.rand * 0.001, 0, mod * 20.rand),
					0.5, 1/n * amp * env),
		].wchoose([0.15, 0.25, 0.5]);
	});
	out = Mix([env * out * (1 - delayMix),
		CombN.ar(env * out, 10, 1.1, 1 * 50) * delayMix]);
	out = LPF.ar(out, 8000);
	Out.ar([0, 1], out * vol);
}).add;

SynthDef(\eideticSpaceNoise, { |bus=0, amp=0.05, attack=1, gate, release=1|
	var env, carrier, mod, modPrime, panMod, freq, out, fader;
	env = EnvGen.kr(Env.adsr(attack,5,5,release, curve:\sine), gate:gate);
	panMod = SinOsc.ar(1/40);
	modPrime = SinOsc.ar([1/70, 1/70 + (1/70 * panMod)], 0, 15, 20);
	fader = SinOsc.ar(_);
	mod = LinXFade2.ar(
		LinXFade2.ar(SinOsc.ar(modPrime), Pulse.ar(modPrime), fader.(1/28)),
		LinXFade2.ar(LFTri.ar(modPrime* 80), Saw.ar(modPrime), fader.(1/28)),
		fader.(1/50);
	);
	freq = SinOsc.ar(1/120, 0, 2000, 3000);
	carrier = SinOsc.ar(freq * mod);
	out = carrier;
	out = out + AllpassN.ar(out, 0.7, 0.7, 10);
	Out.ar(bus, amp * env * out ! 2);
}).store;

SynthDef(\eideticWhiteNoise, {
	|bus=0, amp=0.05, attack=1, gate, release=1, lag, spread=1|
	var out, env, bpf, lfo, sig = In.ar(bus, 2);
	var len = 0.1;
	env = EnvGen.kr(Env.adsr(attack,5,5,release, curve:\sine), gate:gate);
	lfo = SinOsc.ar(0.05, 0, spread * 2000, 2100);
	out = WhiteNoise.ar * amp * env;
	out = BPF.ar(out, lfo, 0.4);
	out = out + AllpassN.ar(out, 0.7, 0.7, 10);
	Out.ar(bus, out ! 2);
}).store;

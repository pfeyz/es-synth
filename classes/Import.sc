Import {
	classvar <>base;
	*new { |fn, interpreter, prefix|
		var env, path = (prefix ? base ? "") ++ fn ++ ".sc";
		env = Environment.make(interpreter.compileFile(path));
		env.know = true;
		^env;
	}
}

var gui, synthDefs;
var activePad;
var controlToOrd = Dictionary[ (1 -> 10), (7 -> 9), (74 -> 1),
	(93 -> 4), (71 -> 2), (84 -> 8), (91 -> 3),
	(5 -> 7), (72 -> 6), (73 -> 5) ];
var envScale = 20;
var keyLookup = ();
var currentVoice = 0;

Import.base = thisProcess.nowExecutingPath.dirname +/+ "/";

~leadSynth = {
	var lead = Import("eidetic", this);
	var leadSemaphore = Semaphore(1);
	var leadNoteOn = { |src, chan, num, vel|
		[\lead, num, \on].postln;
		leadSemaphore.wait;
		lead.osc.set(\freq, midicps(num + 12), \gate, 1);
		lead.notecount = lead.notecount + 1;
		leadSemaphore.signal;
	};

	var leadNoteOff = { |src, chan, num, vel|
		leadSemaphore.wait;
		lead.notecount = lead.notecount - 1;
		lead.notecount.postln;
		if(lead.notecount <= 0) {
			"gate 0".postln;
			lead.notecount = 0;
			lead.osc.set(\gate, 0);
		};
		leadSemaphore.signal;
	};

	NoteOnResponder(leadNoteOn, chan: 1);
	NoteOffResponder(leadNoteOff, chan: 1);

	lead;
};

~padSynth = {
	var pad = Import("patch", this);
	var padSemaphore = Semaphore(1);
	var padNoteOn = { |src, chan, num, vel|
		var cur;
		padSemaphore.wait;
		currentVoice = currentVoice + 1;
		if (currentVoice >= pad.numVoices) {
			currentVoice = 0;
		};
		currentVoice.postln;
		cur = pad.voices[currentVoice];
		if (cur.isNil.not){
			num.postln;
			cur.osc.set(\freq, midicps(num));
			cur.env.set(\gate, 1);
			keyLookup[num] = cur;
		};
		padSemaphore.signal;
	};

	var padNoteOff = { |src, chan, num, vel|
		padSemaphore.wait;
		keyLookup[num].env.set(\gate, 0);
		padSemaphore.signal;
	};
	NoteOnResponder(padNoteOn, chan: 0);
	NoteOffResponder(padNoteOff, chan: 0);
	pad.set(\amp, 0.5);
	pad;
};

~main = {
	var constants = Import("constants", this);
	var toggle = {|node, param|
		node.get(param) { |val|
			if(val == 0){ val = 1}{ val = 0};
			["val", val].postln;
			node.set(param, val);
		}
	};
	var expLead = [0, 1, \sin, 1/1000].asSpec;
	var fadePxSpec = \fadePx.asSpec;
	var freqSpec = \freq.asSpec;
	var rqSpec = \rq.asSpec;
	var lofreqSpec = \lofreq.asSpec;

	CCResponder.removeAll;
	~lead = ~leadSynth.();
	~pad = ~padSynth.();
	~knobControls = (
		// knobs
		1: \padAttack,
		2: \padRelease,
		3: \leadDelayMix,
		4: \leadDelayVol,
		5: \padFiltFreq,
		6: \padFiltRes,
		7: \lfoMix,
		8: \lfoDepth,

		9: \volume,

		// modwheel
		10: \lfoFreq,

		// pads
		13: \toggleWhiteNoise,
		14: \toggleSpaceNoise,
		16: \killAll,
		17: \killSwitch,
		18: \lfoMod,

		113: \loop,
		114: \back,
		115: \forward,
		116: \stop,
		117: \play,
		118: \record
	);

	~shutdown = (
		flags: [\loop, \back, \forward, \stop, \play, \record],
		flagSet: Set(),
		toggleFlag: { |self, name, val|
			if(val == 0, {
				self.flagSet.remove(name);
			}, {
				self.flagSet.add(name);
			});
			if(self.flagSet.size == self.flags.size, {
				Pipe("sudo poweroff", "r");
			});
		});
	~shutdown.know = true;
	~knobHandler = { |src, chan, num, val|
		var db = \db.asSpec;
		var timer = nil;
		var controlName = ~knobControls.at(num);
		val = val / 127;

		if(~shutdown.flags.includes(controlName),
			{
				~shutdown.toggleFlag(controlName, val);
			});

		controlName.switch(
			// pad controls
			\padAttack, {~pad.env.set(\attack, fadePxSpec.map(val) * 2)},
			\padRelease, {~pad.env.set(\release, fadePxSpec.map(val) * 2)},
			\padFiltFreq, {~pad.filter.set(\freq, freqSpec.map(val))},
			\padFiltRes, {~pad.filter.set(\res, rqSpec.map(val))},
			\lfoMix, { // pad pitch modulation vs filter freq
				~pad.filter.set(\lfoAmt, val);
				~pad.osc.set(\lfoAmt, 1 - val);
			},
			\lfoFreq, {~pad.lfo.set(\freq, lofreqSpec.map(val) - 0.1)},
			\lfoDepth, {
				~pad.lfo.set(\depth, expLead.map(val) - 0.01);
			},
			\lfoMod, {
				if(val == 0,
					{ ~pad.lfo.set(\freq, 0) },
					{ ~pad.lfo.set(\freq, 8) });
			},
			// lead controls
			\leadDelayMix, {~lead.osc.set(\delayMix, expLead.map(val));},
			\leadDelayVol, {~lead.osc.set(\vol, expLead.map(val));},

			\toggleWhiteNoise, { if (val == 0){ toggle.(~lead.noiseOsc, \gate); } },
			\toggleSpaceNoise, { if (val == 0){ toggle.(~lead.spaceOsc, \gate); } },

			\killSwitch, {
				if(val == 0,
					{
						if(~killSwitchPreviousVolume.isNil.not,
							{ s.volume.volume_(~killSwitchPreviousVolume); });
						~killSwitchPreviousVolume = nil;
					},
					{
						if(~killSwitchPreviousVolume.isNil,
							{ ~killSwitchPreviousVolume = s.volume.volume; });
					s.volume.volume_(-inf);
					});
			},

			\volume, {s.volume.volume_(db.map(val))},
			\killAll, {
				~lead.notecount = 0;
				~lead.osc.set(\gate, 0);
			},
			{
				["unhandled knob number " ++ num, controlName].postln;
			}
		);
	};
	CCResponder(~knobHandler);
	{ WhiteNoise.ar * 0.3 * EnvGen.ar(Env.perc(0, 1), doneAction: 2); }.play;
};

s.waitForBoot {
	synthDefs = Import("synths", this);
	MIDIClient.init;
	MIDIIn.connect(0, 3);
	~main.();
}
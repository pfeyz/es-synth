~eidetic = Group.new();
~osc = Synth.head(~eidetic, \eideticSynth,
	[\freq, 200, \amp, 0.5, \gate, 0]);

~spaceOsc = Synth.head(~eidetic, \eideticSpaceNoise,
	[\amp, 0.05, \attack, 5, \release, 20]);
~noiseOsc = Synth.head(~eidetic, \eideticWhiteNoise,
	[\amp, 0.1, \attack, 5, \release, 20]);

~osc.set(\vibRate, 0.01);
~osc.set(\vibDepth, 0.05);
~osc.set(\release, 25);
~notecount = 0;
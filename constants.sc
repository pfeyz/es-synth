~padControlChan = 0;
~leadControlChan = 1;
~noteChan = 1;
~padChan = 9;
~wheelChan = 0;
~wheelToOrd = Dictionary[ (1 -> 9), (7 -> 8), (74 -> 0),
	(93 -> 3), (71 -> 1), (84 -> 7), (91 -> 2),
	(5 -> 6), (72 -> 5), (73 -> 4) ];
~buttonName = Dictionary();
[\loop, \back, \forward, \stop, \play, \record].do { |name, index|
	~buttonName[index + 113] = name;
};
~padToOrd = Dictionary();
[36, 38, 42, 46, 50, 45, 51, 49].do { |num, index|
	~padToOrd[num] = index;
}

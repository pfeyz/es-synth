var spec = ControlSpec(0, 127, \linear);

~show = {
	var window = Window().front;
	var knobs;
	var makeKnobs = { |labels, offset|
		labels.collect { |label, n|
			EZKnob(window, label:label, controlSpec: spec, action: { |k|
				~ccResp.(num: n + offset, val: k.value);
			});
		}
	};
	window.addFlowLayout;
	knobs = makeKnobs.(["attack", "decay", "sustain", "release"], 1);
	window.view.decorator.nextLine;
	knobs = knobs ++ makeKnobs.(["vcf freq", "vcf res", "vcf lfo", "lfo freq"], 5);
};
